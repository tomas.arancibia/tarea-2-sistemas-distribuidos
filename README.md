# Tarea 2 Sistemas Distribuidos

El presente repositorio contiene los archivos necesarios para la resolucion de la tarea 2, junto con las instrucciones de uso en el archivo readme.
El trabajo fue desarollado con Nodejs (javaScript)
Integrantes del grupo: Tomás Arancibia y Benjamín Devia.

## Instrucciones para ejecutar los contenedores docker

Una vez descargado el trabajo, se debe acceder al directorio Tarea2 mediante la terminal. Una vez dentro, se debe ejecutar el docker-compose para levantar los servicios: 

`sudo docker-compose up -d`

Al ejecutar el comando, este creará las imagenes de los servicios, y luego los levantará en contenedores. Para verificar que se encuentran funcionando los 4 servicios, se recomienda utilizar el comando:

`sudo docker ps`

Este nos muestra todos los contenedores en que se encuentran activos. Por lo cual, al ejecutarlo, deben aparecer los siguientes contendores levantados:
    - zookeeper
    - kafka
    - login-service
    - security-service

Esto nos indicará que el sistema esta activo y listo para usar.

## Instrucciones de funcionamiento del sistema

El sistema se compone principalmente de cuatro servicios. Dos de ellos (zookeeper y kafka), son levantados principalmente para el uso de Kafka. Mientras que los otros dos (login-service y security-service), son los encargadados de resolver la problematica del presente trabajo. Basicamente, estos dos ultimos, son APIs REST construidas en Nodejs, las cuales interactuan entre si utilizando Kafka como intermediario.
Para poder acceder y utilizar los dos servicios principales, se debe tener en consideracion:

- Login Service: Este servicio se ejecuta en el puerto 3000 del localhost, y para enviarle datos (user y password), se debe realizar una solicitud POST a la ruta /login, con la data incluida en el Body (se recomienda el uso una plataforma de teste de APIs como Postman).

`http://localhost:3000/login [POST]`

donde la data del Body, deber ser por ejemplo:

>{
>    "user": "User_example",
>    "password": "Password_example"
>}

Nuestra propuesta de solucion no posee una base de datos como tal para almacenar usuarios, por ende, la primera solicitud que se envie al sistema de logueo, seteará el usuario y contraseña en un Array, el cual se utilizará para almacenar a los n usuarios, junto a sus contraseñas. Por otro lado, para almacenar a los usuarios bloqueados, se hizo uso de una archivo (db/users_blocked.json) .JSON, el cual es compartido entre el login-service (solo lee) y el security-service (lee y escribe).

Si es que la contraseña del usuario es correcta (o en su defecto, se seteo un nuevo usuario), se obtendra un resultado como este:
>{
>    "login_status": "Exitoso! "
>}

En el caso en que la contraseña sea erronea, se obtendra un resulado como este: 

>{
>    "login_status": "Falló ",
>    "error": "contraseña incorrecta"
>}

Ahora bien, si el sistema detecta 5 inicios de sesión erroneos consecutivos y en menos de 1 minuto, para un usuario determinado, el sistema de seguridad bloqueará automaticamente la cuenta, por ende, al volver a intentar loguearse, correcta o no la contraseña, se mostrará el siguiente mensaje:

>{
>    "login_status": "Falló ",
>    "error": "user blocked"
>}

Indicando que su cuenta está bloqueada.

- Security Service: Este servicio se ejecuta en el puerto 5000 del localhost. Para poder acceder a este servicio, se debe realizar una solicitud GET a la ruta /blocked, para obtener el listado de los usuarios bloqueados:

`http://localhost:5000/blocked [GET]`

Como resultado, se obtendrá lo siguiente: 

>{
>    "users_blocked": [
>        "benja",
>        "tomi"
>    ]
>}

donde los usuarios "benja" y "tomi" estan bloqueados.

En caso de que no se encuentren usuarios bloqueados, se obtendra: 

>{
>    "users_blocked": []
>}

## Preguntas Kafka

1) ¿Por qué Kafka funciona bien en este escenario?.

R: Por que se acomoda de gran manera a las funcionalidades que entrega el software, bajo una arquitectura distribuida. En cierto modo se genera una ruta (acceso) mucho más corta de comunicación entre servicios, ya que kafka facilita el flujo de datos entre ambos en tiempo real (ingreso y extraccón de datos desde el tópico).

2) Basado en las tecnologías que usted tiene a su disposicíon (Kafka, backend) ¿Que harıa usted para manejar una gran cantidad de usuarios al mismo tiempo?.

R: La mejor forma sería aplicar tecnicas de escalamiento horizontal al sistema, es decir, agregar replicaciones, ya que con esas replicas se generá una tolerancia mucho mayor para uso al sistema (mayor disponibilidad), evitando en gran medida cuellos de botella, haciendolo además tolerante a fallos (ya que las replicas cubririan los huecos que dejan las copias que se lleguen a caer).
